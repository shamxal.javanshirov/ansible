# Playbook Description

This playbook installs software and configures system settings on a RHEL based server. It consists of two roles: `software_installation` and `system_conf_role`. Each role performs specific tasks and has its own set of variables.

## Role: software_installation

This role installs Docker and Kubernetes packages on the server.

### Variables

The following variables are defined for this role:

```software installation roles variables yaml
docker_repo: https://download.docker.com/linux/centos/docker-ce.repo
docker_packages:
  - docker-ce
  - docker-ce-cli
  - containerd.io
  - docker-buildx-plugin
  - docker-compose-plugin
  - wget
k8s_packages:
  - kubectl
  - kubeadm
  - kubelet
VER: 1.26.1
http_proxy: http://proxy_server_ip:port
https_proxy: http://proxy_server_ip:port


```system conf roles variables yaml
proxy_path: http://proxy_server_ip:port
packages:
  - iproute-tc
  - vim
  - net-tools
  - bind-utils
  - tcpdump
cri_modules:
  - overlay
  - br_netfilter
sysctl_parametrs:
  - net.bridge.bridge-nf-call-iptables = 1
  - net.ipv4.ip_forward = 1
  - net.bridge.bridge-nf-call-ip6tables = 1


To modify these variables, you can edit the playbook file or override them during playbook execution by passing new values as extra variables.
